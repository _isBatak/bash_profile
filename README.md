# bash_profile for Git

Kopirati .bash_profil u "User Directory" C:\Users\USER_NAME

# Objasnjenje

Use echo command to display current BASH prompt:
[test@fedora ~]$ echo $PS1

--

To add colors to the shell prompt use the following command syntax:
'\e[x;ym $PS1 \e[m'

--

Where,

\e[ Start color scheme

x;y Color pair to use (x;y)

$PS1 is your shell prompt

\e[m Stop color scheme

Let's consider we have two machine "Fedora-1" & "Fedora-2" and to differentiate between these to machine we need following settings:

1] When a user login to Fedora-1 machine the prompt color change to green and

2] When a user login to Fedora-2 machine the prompt color change to red.

To achieve this follow the steps given below:

For Fedora-1, add following line at the end in /etc/bashrc or /etc/profile

[root@fedora-1 ~]# vim /etc/bashrc

PS1="\e[0;32m[\u@\h \W]\$ \e[m "

For Fedora-2, add following line at the end in /etc/bashrc or /etc/profile

[root@fedora-2 ~]# vim /etc/bashrc

PS1="\e[0;31m[\u@\h \W]\$ \e[m "

After making above settings, based on the prompt color users can identify on which machine they are currently login. Fedora-1 machine primary prompt color changed to green color & Fedora-2 machine primary prompt color changed to red color after login to the system.

Note: The above setting is global and it is applied to each user who are login to the system.

To set the prompt color based on user profile then set the PS1 value in ~/.bashrc or ~/.bash_profile file which are located in users home directory.

Eg:

[test@Fedora-1 ~]$ vim /home/test/.bashrc

PS1="\e[0;33m[\u@\h \W]\$ \e[m "

We can also set the BASH primary prompt background color along with prompt color.

Eg;

--

[test@Fedora-1 ~]# PS1="\e[0;30;103m[\u@\h \W]\$ \e[m "

--

Refer the following color coding for shell prompt.

Regular color settings for prompt character:

--

Black='\e[0;30m' # Black

Red='\e[0;31m' # Red

Green='\e[0;32m' # Green

Yellow='\e[0;33m' # Yellow

Blue='\e[0;34m' # Blue

Purple='\e[0;35m' # Purple

Cyan='\e[0;36m' # Cyan

White='\e[0;37m' # White

--

Regular color settings with bold character:
BBlack='\e[1;30m' # Black

BRed='\e[1;31m' # Red

BGreen='\e[1;32m' # Green

BYellow='\e[1;33m' # Yellow

BBlue='\e[1;34m' # Blue

BPurple='\e[1;35m' # Purple

BCyan='\e[1;36m' # Cyan

BWhite='\e[1;37m' # White

--

Regular color settings with underline characters:
UBlack='\e[4;30m' # Black

URed='\e[4;31m' # Red

UGreen='\e[4;32m' # Green

UYellow='\e[4;33m' # Yellow

UBlue='\e[4;34m' # Blue

UPurple='\e[4;35m' # Purple

UCyan='\e[4;36m' # Cyan

UWhite='\e[4;37m' # White

--

Only background color settings of the prompt:
On_Black='\e[40m' # Black

On_Red='\e[41m' # Red

On_Green='\e[42m' # Green

On_Yellow='\e[43m' # Yellow

On_Blue='\e[44m' # Blue

On_Purple='\e[45m' # Purple

On_Cyan='\e[46m' # Cyan

On_White='\e[47m' # White

--

Regular color for prompt character with High Intensity :
IBlack='\e[0;90m' # Black

IRed='\e[0;91m' # Red

IGreen='\e[0;92m' # Green

IYellow='\e[0;93m' # Yellow

IBlue='\e[0;94m' # Blue

IPurple='\e[0;95m' # Purple

ICyan='\e[0;96m' # Cyan

IWhite='\e[0;97m' # White

--

Bold prompt character with High Intensity :
BIBlack='\e[1;90m' # Black

BIRed='\e[1;91m' # Red

BIGreen='\e[1;92m' # Green

BIYellow='\e[1;93m' # Yellow

BIBlue='\e[1;94m' # Blue

BIPurple='\e[1;95m' # Purple

BICyan='\e[1;96m' # Cyan

BIWhite='\e[1;97m' # White

--

High Intensity backgrounds colors:
On_IBlack='\e[0;100m' # Black

On_IRed='\e[0;101m' # Red

On_IGreen='\e[0;102m' # Green

On_IYellow='\e[0;103m' # Yellow

On_IBlue='\e[0;104m' # Blue

On_IPurple='\e[10;95m' # Purple

On_ICyan='\e[0;106m' # Cyan

On_IWhite='\e[0;107m' # White

DARKGRAY='\e[1;30m'
LIGHTRED='\e[1;31m'
GREEN='\e[32m'
YELLOW='\e[1;33m'
LIGHTBLUE='\e[1;34m'
NC='\e[m'